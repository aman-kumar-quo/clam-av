const NodeClam = require('clamscan');
const path = require('path')

const options = {
    debug_mode: true,
    scan_recursively: false,
    clamdscan: {
        socket: '/var/run/clamav/clamd.ctl',
        timeout: 120000,
        local_fallback: true,
        path: '/var/lib/clamav',
        config_file: '/etc/clamav/clamd.conf',
        multiscan: true,
    },
};

const print = (param, ...optionalParams) => {
    console.log(param, optionalParams);
}

const run = async () => {
    try {
        // const file_name = 'Shubham_Bansal_SE.pdf.jpg';
        // const file_name = 'eicar.com.jpg';
        // const file_name = 'pdf-doc-vba-eicar-dropper.pdf';
        // const file_name = 'test.php';
        const file_name = 'achi_pic.jpg';

        const clamscan = await new NodeClam().init(options);

        const folder = path.join(process.cwd(), 'scan');

        // print({ folder });

        // const result_of_dir = await clamscan.scan_dir(folder);
        // print({ result_of_dir });

        const result_of_file = await clamscan.scan_files(path.join(folder, file_name));
        print({ result_of_file });

    } catch (err) {
        print('catch', { err });
    }
}

run()